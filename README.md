# recoda

This script aims to provide an easy tool to create a new hierarchy and names after a "dirty" data recovery.

This software does not lock files for parallel access. This can cause problems (but generally not), you can also 
run the tools multiple times to be sure that everything has been copied. This will be CPU intensive since hashes are
being used to distingish similar sized files.

Dependencies needed:
- exiftool
- parallel (optionnal)

Usage: 

To use it in parallel (much faster)
find "path/to/source" -type f | parallel --eta -j NUMBER_OF_RUNS -k path/to/recoda [OPTIONS] "{}" "path/to/dest"

To run it sequentially
find "path/to/source" -type f | xargs path/to/recoda [OPTIONS] "{}" "path/to/dest"

Options:  
    -v  display information about file transfer  
    -vv display more information
    -i  only analyze and copy images

Good luck!

