#! /bin/bash
# Usage: recoverPictures SRC DEST
# This algorithm is not optimal, I'll let the reader write it in C if needed
# Author: Clyde Laforge
# Version 1.2 (20.02.2019)
# Intended usage with parallel:
#    find "$SRC" -type f | parallel --eta -j"$N" -k process_file "{}" "$DEST"
# Lists

VERBOSE="false"
HIGH_VERBOSE="false"
ONLY_IMAGE="false"
ONLY_MUSIC="false"
EVERYTHING="false"

PICTURES="jpg png mov mp4 mkv wmv raw cr2 cr3 divx jpeg jng bmp gif avi m2ts"
MUSIC="mp3 wav aiff aac flac m4a wma mkv"
#DOCUMENT="pdf odt odf doc docx mobi ods pages zip"

MAX_NAME_SIZE=255

# Functions

check_dependencies(){
    if ! command -v exiftool > /dev/null; then
        echo "This script depends on libimage-exiftool-perl (exiftool), please install it"
        echo "Ubuntu: apt install libimage-exiftool-perl"
        echo "Fedora: dnf install perl-Image-ExifTool.noarch"
        exit 1
    fi
}

init()
{
    if [ "$#" -lt 2 ] || ! [ -d "${@: -1}" ] || ! [ -f "${@:(-2):1}" ] ; then
        printf "Usage: recoverPictures [OPTIONS] SRC DEST\\nDo directories exists?\\n"
        exit 1
    fi

    while [ "$3" != "" ]; do
        case $1 in
            -v | --verbose )
                shift
                VERBOSE="true"
                ;;
            -vv )
                shift
                HIGH_VERBOSE="true"
                VERBOSE="true"
                ;;
            -i )
                shift
                ONLY_IMAGE="true"
                ;;
            -m )
                shift
                ONLY_MUSIC="true"
                ;;
            -e )
                shift
                EVERYTHING="true"
                ;;
            * )
                exit 1
        esac
    done


    # ${@: -1} take the last parameter
    if [[ "$2" == /* ]] ; then
        DEST="$2"
    else
        DEST=$(readlink -f "$2")
    fi

    if [[ "$1" == /* ]]; then
        INPUT_FILE="$1"
    else
        INPUT_FILE="$(readlink -f "$1")"
    fi
}

execute(){

    NEW_NAME="NO"
    DIR=$DEST

    # extraction file type
    filename="$(basename -- "$INPUT_FILE")"
    extension=$(echo "${filename##*.}" | tr '[:upper:]' '[:lower:]' )
    filename="${filename%.*}"
    printf "\\r"

    if [[ $HIGH_VERBOSE == true ]]; then
        printf "%s\\n" "$INPUT_FILE"
    fi


    # Chose filename and destination
    if [[ $PICTURES =~ (^|[[:space:]])$extension($|[[:space:]]) ]] && [[ "$ONLY_MUSIC" == "false" ]]; then
        # metadata accesible?
        TEMP=$(exiftool -createDate "$INPUT_FILE" 2>/dev/null)
        if [[ $TEMP != '' ]] && [[ $TEMP != '-' ]]; then
            DIR="$DEST/Pictures/$(exiftool -T -d "%Y/%m" -createDate "$INPUT_FILE")"
            NEW_NAME="$(exiftool -T -d "%Y:%m:%d:%T" -createDate "$INPUT_FILE")"

        fi

        TEMP=$(exiftool -profileDateTime "$INPUT_FILE" 2>/dev/null)
        if [[ $TEMP != '' ]] && [[ $TEMP != '-' ]] && [[ "$NEW_NAME" = "NO" ]]; then
            DIR="$DEST/Pictures/ProfileDate"
            NEW_NAME="$filename"
        fi

    elif [[ $MUSIC =~ (^|[[:space:]])$extension($|[[:space:]]) ]] && [[ $ONLY_IMAGE = "false" ]]; then
        title=$(exiftool -T -Title "$INPUT_FILE" 2>/dev/null)
        if [[ "$title" != "" ]] && [[ $title != '-' ]]; then
            if echo $(exiftool -T -Track "$INPUT_FILE") | egrep -q '^[0-9]+$'; then
                NEW_NAME=$(printf "%02d" $(exiftool -T -Track "$INPUT_FILE"))" - ""$title"
            elif echo $(exiftool -T -TrackNumber "$INPUT_FILE") | egrep -q '^[0-9]+$'; then
                tmp=$(exiftool -T -TrackNumber "$INPUT_FILE")
                NEW_NAME=$(printf "%02d" "${tmp#0}")" - ""$title"
            else
                NEW_NAME="$title"
            fi

            artist=$(exiftool -T -Artist "$INPUT_FILE" | cut -c -"$MAX_NAME_SIZE")
            artist=${artist#\//|}
            album=$(exiftool -T -Album "$INPUT_FILE" | cut -c -"$MAX_NAME_SIZE")
            album=${album#\//|}
            if [[ "$album" != "" ]] && [[ "$artist" != "" ]] && [[ "$album" != '-' ]] && [[ "$artist" != '-' ]]   ; then
                DIR="$DEST/Music/$artist/$album"
            else
                DIR="$DEST/Music/OnlyName"
            fi
        fi
    elif [[ $DOCUMENT =~ (^|[[:space:]])$extension($|[[:space:]]) ]] && [[ $ONLY_IMAGE = "false" ]]; then
        title=$(exiftool -T -Title "$INPUT_FILE" 2>/dev/null)
        if [[ "$title" != "" ]] && [[ $title != '-' ]] && [[ $title != "Sans titre" ]]; then
            nb_in_docs=$((nb_in_docs+1))
            nb_recovered=$((nb_recovered+1))
            if [[ $nb_in_docs -gt 100 ]]; then
                nb_in_docs=0;
                nb_folder_in_docs=$((nb_folder_in_docs+1))
            fi
            NEW_NAME="$title"
            DIR="$DEST/Documents/Folder$nb_folder_in_docs"
            #  NEW_NAME="$filename"
            #  DIR="$DEST/Documents/Unsorted"
        fi
    fi

    #Should we copy everything?
    if [[ $EVERYTHING = "false" ]] && [ "$NEW_NAME" = "NO" ]; then
        exit 0
    elif [ "$NEW_NAME" = "NO" ]; then
        DIR="$DEST/Misc"
        NEW_NAME="$filename"
    fi

    # Sanitize name
    NEW_NAME=$(echo "$NEW_NAME" | cut -c -"$MAX_NAME_SIZE" | sed 's,\(/\||\|\\\),,g')
    mkdir -p "$DIR"

    # Copy file carefully (duplication,...)
    temp=""
    temp_num=1
    same=false
    while [ -e "$DIR/$NEW_NAME$temp.$extension" ]
    do
        if [ "$(stat --format=%s "$INPUT_FILE")" -gt 50000000 ] && [[ "$(stat --format=%s "$DIR/$NEW_NAME$temp.$extension")" = $( stat --format=%s "$INPUT_FILE") ]]; then
            same=true
        elif [[ $(md5sum "$INPUT_FILE" | awk '{print $1}') = $(md5sum "$DIR/$NEW_NAME$temp.$extension" | awk '{print $1}') ]]; then
            same=true
        fi

        if [ $same = "true" ]; then
            if [ $VERBOSE = true ];then
                echo "File $DIR/$NEW_NAME$temp.$extension already exists, not copying"
            fi
            exit 0
        fi
        temp="_$temp_num"
        temp_num=$((temp_num+1))

    done

    if [[ $VERBOSE == "true" ]]; then
        printf "Copying %s to %s\\n" "$INPUT_FILE" "$DIR/$NEW_NAME$temp.$extension"
    fi
    cp --backup "$INPUT_FILE" "$DIR/$NEW_NAME$temp.$extension"
}


check_dependencies
init "$@"
execute


exit 0
